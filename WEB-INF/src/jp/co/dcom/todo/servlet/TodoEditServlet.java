package jp.co.dcom.todo.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.dcom.todo.dto.Todo;
import jp.co.dcom.todo.service.TodoService;

@WebServlet(name = "TodoEditServlet", urlPatterns = { "/edit" })
public class TodoEditServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// TODOIdを取得
		String todoId = req.getParameter("id");

		// TODO情報を取得
		TodoService todoService = new TodoService();
		Todo todo = todoService.findById(todoId);

		req.setAttribute("todo", todo);

		req.getRequestDispatcher("WEB-INF/jsp/edit.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// リクエストパラメータの文字エンコーディング
		req.setCharacterEncoding("UTF-8");

		// 登録するためのDTOを生成
		Todo todo = new Todo();
		todo.setId(Integer.parseInt(req.getParameter("id")));
		todo.setTodo(req.getParameter("todo"));
		todo.setPriority(req.getParameter("priority"));
		todo.setLimit(req.getParameter("limit"));

		TodoService todoService = new TodoService();

		List<String> message = todo.validate();

		if(!message.isEmpty()){
			// JSPに渡すパラメータを設定
			req.setAttribute("message", message);
			req.setAttribute("todo", todo);

			req.getRequestDispatcher("WEB-INF/jsp/edit.jsp").forward(req, resp);
			return;
		}

		// 登録
		todoService.edit(todo);

		// 一覧画面を再表示（リダイレクト）
		resp.sendRedirect("list");
	}

}
