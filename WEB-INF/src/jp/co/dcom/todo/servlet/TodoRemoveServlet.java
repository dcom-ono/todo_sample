package jp.co.dcom.todo.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.dcom.todo.service.TodoService;

@WebServlet(name = "TodoRemoveServlet", urlPatterns = { "/remove" })
public class TodoRemoveServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// TODOIdを取得
		String todoId = req.getParameter("id");

		// TODO情報を取得
		TodoService todoService = new TodoService();
		todoService.remove(todoId);

		// 一覧画面を再表示（リダイレクト）
		resp.sendRedirect("list");
	}
}
