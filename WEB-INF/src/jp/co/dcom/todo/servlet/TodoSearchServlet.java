package jp.co.dcom.todo.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.dcom.todo.dto.Condition;
import jp.co.dcom.todo.dto.Todo;
import jp.co.dcom.todo.service.TodoService;

@WebServlet(name = "TodoSearchServlet", urlPatterns = { "/search" })
public class TodoSearchServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// リクエストパラメータの文字エンコーディング
		req.setCharacterEncoding("UTF-8");

		Condition condition = new Condition();
		condition.setTodo(req.getParameter("todo"));
		condition.setPriority(req.getParameterValues("priority"));
		condition.setLimitStart(req.getParameter("limitStart"));
		condition.setLimitEnd(req.getParameter("limitEnd"));

		TodoService todoService = new TodoService();

		// 登録
		List<Todo> todoList = todoService.findByCondition(condition);

		// JSPに渡すパラメータを設定
		req.setAttribute("todoList", todoList);
		req.setAttribute("condition", condition);

		// 画面表示処理をJSPに委譲
		req.getRequestDispatcher("WEB-INF/jsp/list.jsp").forward(req, resp);

	}

}
