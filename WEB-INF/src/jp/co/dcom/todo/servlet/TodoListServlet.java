package jp.co.dcom.todo.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.dcom.todo.dto.Todo;
import jp.co.dcom.todo.service.TodoService;

@WebServlet(name = "TodoListServlet", urlPatterns = { "/list" })
public class TodoListServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		TodoService todoService = new TodoService();

		// TODOリストを取得
		List<Todo> todoList = todoService.findAll();

		// JSPに渡すパラメータを設定
		req.setAttribute("todoList", todoList);

		// 画面表示処理をJSPに委譲
		req.getRequestDispatcher("WEB-INF/jsp/list.jsp").forward(req, resp);
	}

}
