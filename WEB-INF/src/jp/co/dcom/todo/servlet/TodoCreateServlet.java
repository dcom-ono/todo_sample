package jp.co.dcom.todo.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.dcom.todo.dto.Todo;
import jp.co.dcom.todo.service.TodoService;

@WebServlet(name = "TodoCreateServlet", urlPatterns = { "/create" })
public class TodoCreateServlet extends HttpServlet {

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		// リクエストパラメータの文字エンコーディング
		req.setCharacterEncoding("UTF-8");

		// 登録するためのDTOを生成
		Todo todo = new Todo();
		todo.setTodo(req.getParameter("todo"));
		todo.setPriority(req.getParameter("priority"));
		todo.setLimit(req.getParameter("limit"));

		// 入力チェック
		List<String> message = todo.validate();

		TodoService todoService = new TodoService();

		if(!message.isEmpty()){

			// TODOリストを取得
			List<Todo> todoList = todoService.findAll();

			// JSPに渡すパラメータを設定
			req.setAttribute("message", message);
			req.setAttribute("todoList", todoList);
			req.setAttribute("todo", todo);

			// 画面表示処理をJSPに委譲
			req.getRequestDispatcher("WEB-INF/jsp/list.jsp").forward(req, resp);

			return;
		}

		// 登録
		todoService.create(todo);

		// 一覧画面を再表示（リダイレクト）
		resp.sendRedirect("list");
	}

}
