package jp.co.dcom.todo.service;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import jp.co.dcom.todo.dto.Condition;
import jp.co.dcom.todo.dto.Todo;
import jp.co.dcom.todo.util.DateUtil;
import jp.co.dcom.todo.util.DbUtil;
import jp.co.dcom.todo.util.StringUtil;

/**
 * TODO情報に関する業務ロジック
 *
 * @author 2012DELL-20
 *
 */
public class TodoService {

	/**
	 * TODO情報を全て取得し、リストで返却します。
	 * @return TODOリスト
	 */
	public List<Todo> findAll(){
		DbUtil db = new DbUtil();
		List<Todo> todoList = new ArrayList<Todo>();

		try {
			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("SELECT id,todo,priority,limit_date FROM todo ORDER BY priority");

			// クエリ実行
			ResultSet rs = ps.executeQuery();

			// ResultSetをListに格納
			while(rs.next()){
				Todo todo = new Todo();
				todo.setId(rs.getInt("id"));
				todo.setTodo(rs.getString("todo"));
				todo.setPriority(rs.getString("priority"));
				todo.setLimit(DateUtil.convertDate2String(rs.getDate("limit_date")));
				todoList.add(todo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return todoList;

	}

	public Todo findById(String id){
		DbUtil db = new DbUtil();
		Todo todo = new Todo();

		try {
			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("SELECT id,todo,priority,limit_date FROM todo WHERE id = ?");
			ps.setInt(1, Integer.parseInt(id));

			// クエリ実行
			ResultSet rs = ps.executeQuery();

			// ResultSetをListに格納
			while(rs.next()){
				todo.setId(rs.getInt("id"));
				todo.setTodo(rs.getString("todo"));
				todo.setPriority(rs.getString("priority"));
				todo.setLimit(DateUtil.convertDate2String(rs.getDate("limit_date")));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return todo;
	}

	public List<Todo> findByCondition(Condition condition){
		DbUtil db = new DbUtil();
		List<Todo> todoList = new ArrayList<Todo>();

		try {
			// DB接続
			db.connect();

			// SQL文生成
			String sql = "SELECT id,todo,priority,limit_date FROM todo WHERE 1=1";
			if(!StringUtil.isEmpty(condition.getTodo())){
				sql+=" AND todo LIKE '%" + condition.getTodo() + "%'";
			}
			if(condition.getPriority() != null && condition.getPriority().length > 0){
				StringBuilder sb = new StringBuilder();
				for(String p : condition.getPriority()){
					if(sb.length() > 0){
						sb.append(",");
					}
					sb.append("'");
					sb.append(p);
					sb.append("'");
				}
				sql += " AND priority IN (" + sb.toString() + ")";
			}
			if(!StringUtil.isEmpty(condition.getLimitStart())){
				sql+=" AND limit_date >= '" + condition.getLimitStart() + "'";
			}
			if(!StringUtil.isEmpty(condition.getLimitEnd())){
				sql+=" AND limit_date <= '" + condition.getLimitEnd() + "'";
			}
			PreparedStatement ps = db.createStatement(sql);

			// クエリ実行
			ResultSet rs = ps.executeQuery();

			// ResultSetをListに格納
			while(rs.next()){
				Todo todo = new Todo();
				todo.setId(rs.getInt("id"));
				todo.setTodo(rs.getString("todo"));
				todo.setPriority(rs.getString("priority"));
				todo.setLimit(DateUtil.convertDate2String(rs.getDate("limit_date")));
				todoList.add(todo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

		return todoList;
	}

	/**
	 * TODOを新規登録します。
	 * @param todo TODO情報
	 */
	public void create(Todo todo){
		DbUtil db = new DbUtil();
		try {

			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("INSERT INTO todo (todo,priority,limit_date) values(?,?,?)");

			// パラメータ設定
			ps.setString(1, todo.getTodo());
			ps.setString(2, todo.getPriority());
			ps.setDate(3, DateUtil.convertString2Date(todo.getLimit()));


			// クエリ実行
			ps.executeUpdate();

			// コミット
			db.commit();

		}catch (Exception e) {
			try {
				// ロールバック
				db.rollback();
			} catch (SQLException e1) {
			}
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}

	public void edit(Todo todo){
		DbUtil db = new DbUtil();
		try {

			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("UPDATE todo SET todo = ?,priority = ?,limit_date = ? WHERE id = ?");

			// パラメータ設定
			ps.setString(1, todo.getTodo());
			ps.setString(2, todo.getPriority());
			ps.setDate(3, DateUtil.convertString2Date(todo.getLimit()));
			ps.setInt(4, todo.getId());

			// クエリ実行
			ps.executeUpdate();

			// コミット
			db.commit();

		}catch (Exception e) {
			try {
				// ロールバック
				db.rollback();
			} catch (SQLException e1) {
			}
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}

	}

	public void remove(String id){
		DbUtil db = new DbUtil();
		try {

			// DB接続
			db.connect();

			// SQL文生成
			PreparedStatement ps = db.createStatement("DELETE FROM todo WHERE id = ?");

			// パラメータ設定
			ps.setInt(1, Integer.parseInt(id));

			// クエリ実行
			ps.executeUpdate();

			// コミット
			db.commit();

		}catch (Exception e) {
			try {
				// ロールバック
				db.rollback();
			} catch (SQLException e1) {
			}
		}finally{
			try {
				// DB切断
				db.disconnect();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
	}
}
