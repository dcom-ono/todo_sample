package jp.co.dcom.todo.util;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtil {
	public static Date convertString2Date(final String str){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try{
			return convert2SqlDate(sdf.parse(str));
		}catch(ParseException e){
			return null;
		}
	}

	public static String convertDate2String(final Date date){
		if(date == null){
			return "";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		return sdf.format(date);
	}

	public static Date convert2SqlDate(final java.util.Date date){

		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new java.sql.Date(cal.getTimeInMillis());
	}
}
