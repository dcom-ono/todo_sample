package jp.co.dcom.todo.dto;

import java.io.Serializable;


/**
 * TODO情報の永続化クラス
 *
 * @author 2012DELL-20
 *
 */
public class Condition implements Serializable {


	/**
	 * TODO内容
	 */
	private String todo;

	/**
	 * 優先順位
	 */
	private String[] priority;

	/**
	 * 期限
	 */
	private String limitStart;
	private String limitEnd;
	public String getTodo() {
		return todo;
	}
	public String[] getPriority() {
		return priority;
	}
	public String getLimitStart() {
		return limitStart;
	}
	public String getLimitEnd() {
		return limitEnd;
	}
	public void setTodo(String todo) {
		this.todo = todo;
	}
	public void setPriority(String[] priority) {
		this.priority = priority;
	}
	public void setLimitStart(String limitStart) {
		this.limitStart = limitStart;
	}
	public void setLimitEnd(String limitEnd) {
		this.limitEnd = limitEnd;
	}


}
