package jp.co.dcom.todo.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.dcom.todo.util.DateUtil;
import jp.co.dcom.todo.util.StringUtil;


/**
 * TODO情報の永続化クラス
 *
 * @author 2012DELL-20
 *
 */
public class Todo implements Serializable {

	/**
	 * ID（PK）
	 */
	private int id;

	/**
	 * TODO内容
	 */
	private String todo;

	/**
	 * 優先順位
	 */
	private String priority;

	/**
	 * 期限
	 */
	private String limit;

	/**
	 * IDを取得します。
	 * @return ID
	 */
	public int getId() {
		return id;
	}
	/**
	 * IDを設定します。
	 * @param id ID
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * TODO内容を取得します。
	 * @return TODO内容
	 */
	public String getTodo() {
		return todo;
	}
	/**
	 * TODO内容を設定します。
	 * @param todo TODO内容
	 */
	public void setTodo(String todo) {
		this.todo = todo;
	}

	/**
	 * 優先順位を取得します。
	 * @return 優先順位
	 */
	public String getPriority() {
		return priority;
	}

	/**
	 * 優先順位を設定します。
	 * @param priority 優先順位
	 */
	public void setPriority(String priority) {
		this.priority = priority;
	}

	/**
	 * 期限を取得します。
	 * @return 期限
	 */
	public String getLimit() {
		return limit;
	}
	/**
	 * 期限を設定します。
	 * @param limit 期限
	 */
	public void setLimit(String limit) {
		this.limit = limit;
	}

	public List<String> validate(){
		List<String> message = new ArrayList<>();

		if(StringUtil.isEmpty(todo)){
			message.add("タスクを入力してください");
		}

		if(!StringUtil.isEmpty(limit) && DateUtil.convertString2Date(limit) == null){
			message.add("期限はyyyy/MM/ddの形式で入力してください");
		}

		return message;
	}
}
