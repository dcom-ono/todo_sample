<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <title>TODO</title>
  </head>
  <body>
    <div class="container">
      <h1>TODO編集</h1>
      <c:if test="${message.size() > 0 }">
      <div class="alert alert-danger">
        <ul>
        <c:forEach items="${message}" var="msg">
          <li>${msg}</li>
        </c:forEach>
        </ul>
      </div>
      </c:if>
      <form method="post" action="edit" role="form" class="form-inline">
        <div class="form-group">
          <textarea class="form-control" rows="3" name="todo">${todo.todo }</textarea>
          <c:set var="selected1"><c:if test="${todo.priority == '1'}">selected</c:if></c:set>
          <c:set var="selected2"><c:if test="${todo.priority == '2'}">selected</c:if></c:set>
          <c:set var="selected3"><c:if test="${todo.priority == '3'}">selected</c:if></c:set>
          <select name="priority" class="form-control">
              <option value="1" ${selected1}>低</option>
              <option value="2" ${selected2}>中</option>
              <option value="3" ${selected3}>高</option>
          </select>
          <input type="text" name="limit" class="form-control" placeholder="期限" value="${todo.limit}">
          <input type="hidden" name="id" value="${todo.id}">
          <input type="submit" value="保存" class="btn btn-success">
        </div>
      </form>
    </div><!-- /.container -->
  </body>
</html>