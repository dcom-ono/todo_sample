<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <title>TODO</title>
  </head>
  <body>
    <div class="container">
      <h1>TODO</h1>
      <c:if test="${message.size() > 0 }">
      <div class="alert alert-danger">
        <ul>
        <c:forEach items="${message}" var="msg">
          <li>${msg}</li>
        </c:forEach>
        </ul>
      </div>
      </c:if>
      <form method="post" action="create" role="form" class="form-inline">
        <div class="form-group">
          <textarea class="form-control" rows="3" name="todo">${todo.todo }</textarea>
          <c:set var="lowSelected"><c:if test="${todo.priority == '1' }" >selected</c:if></c:set>
          <c:set var="middleSelected"><c:if test="${todo.priority == '2' }" >selected</c:if></c:set>
          <c:set var="highSelected"><c:if test="${todo.priority == '3' }" >selected</c:if></c:set>
          <select name="priority" class="form-control">
              <option value="1" ${lowSelected}>低</option>
              <option value="2" ${middleSelected}>中</option>
              <option value="3" ${highSelected}>高</option>
          </select>
          <input type="text" name="limit" class="form-control" placeholder="期限" value="${todo.limit }">
          <input type="submit" value="作成" class="btn btn-success">
        </div>
      </form>
      <hr/>
      <div class="panel panel-default">
        <div class="panel-heading">絞り込み</div>
        <div class="panel-body">
          <form method="post" action="search" role="form" class="form-horizontal">
             <div class="form-group">
               <label for="todo" class="col-sm-2 control-label">タスク</label>
               <div class="col-sm-10">
                 <input type="text" class="form-control" id="todo" name="todo" value="${condition.todo }" placeholder="タスク">
               </div>
             </div>
             <c:set var="low"/>
             <c:set var="middle"/>
             <c:set var="high"/>
             <c:forEach items="${condition.priority}" var="priority">
             	<c:if test="${priority == '1'}"><c:set var="low">checked</c:set></c:if>
             	<c:if test="${priority == '2'}"><c:set var="middle">checked</c:set></c:if>
             	<c:if test="${priority == '3'}"><c:set var="high">checked</c:set></c:if>
             </c:forEach>
             <div class="form-group">
               <label for="priority" class="col-sm-2 control-label">優先順位</label>
               <div class="col-sm-10">
                 <div class="checkbox-inline">
                   <input type="checkbox" value="1" name="priority" id="low" ${low}>
                   <label for="low">低</label>
                 </div>
                 <div class="checkbox-inline">
                   <input type="checkbox" value="2" name="priority" id="middle" ${middle}>
                   <label for="middle">中</label>
                 </div>
                 <div class="checkbox-inline">
                   <input type="checkbox" value="3" name="priority" id="high" ${high}>
                   <label for="high">高</label>
                 </div>
               </div>
             </div>
             <div class="form-group">
               <label for="limit" class="col-sm-2 control-label">期限</label>
               <div class="col-sm-2">
                 <input type="text" class="form-control" placeholder="From" name="limitStart" value="${condition.limitStart }">
               </div>
               <div class="col-sm-1" style="text-align:center">～</div>
               <div class="col-sm-2">
                 <input type="text" class="form-control" placeholder="To" name="limitEnd" value="${condition.limitEnd }">
               </div>
             </div>
             <div class="pull-right" >
               <input type="submit" value="検索" class="btn btn-primary">
             </div>
          </form>
        </div>
      </div>
      <table class="table">
        <thead>
          <tr>
            <th>内容</th>
            <th>優先度</th>
            <th>期限</th>
            <th></th>
          </tr>
      	</thead>
        <tbody>
      <c:forEach var="todo" items="${ todoList}">
        <tr>
          <td>
            <a href="edit?id=${todo.id}">
              <c:forEach var="str" items="${fn:split(todo.todo,'
')}" ><c:out value="${str}" /><br></c:forEach>
            </a>
          </td>
          <td>
             <c:choose>
                 <c:when test="${todo.priority == '1'}">低</c:when>
                 <c:when test="${todo.priority == '2'}">中</c:when>
                 <c:when test="${todo.priority == '3'}">高</c:when>
             </c:choose>
          </td>
          <td>${todo.limit}</td>
          <td><a class="btn btn-danger" href="remove?id=${todo.id}">削除</a>
        </tr>
      </c:forEach>
        </tbody>
	  </table>
    </div><!-- /.container -->
  </body>
</html>